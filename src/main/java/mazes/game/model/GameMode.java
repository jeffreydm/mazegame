package mazes.game.model;

public enum GameMode
{
	PRECISION(0,10,10,true),
	SPRINT(1,10,10,false),
	REFLEX(2,5,5,true),
	SPRAWL(3,75,37,false),
	BLIND(3,10,10,false);
	
	protected final int type;
	protected final int width;
	protected final int height;
	protected final boolean explosions;
	
	GameMode(int type, int width, int height, boolean explosions)
	{
		this.type = type;
		this.width = width;
		this.height = height;
		this.explosions = explosions;
	}
	
	public int getType()
	{
		return this.type;
	}
	public int getWidth()
	{
		return this.width;
	}
	public int getHeight()
	{
		return this.height;
	}
	public boolean getExplosions()
	{
		return this.explosions;
	}
}