package mazes.game.model;

import mazes.helper.Position;
import mazes.helper.Positionable;

public class Goal implements Positionable
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Goal.class);

	private Position position;
	
	public Goal(Position position)
	{
		logger.info("New Goal created at "+position.toString());
		this.position = position;
	}

	@Override
	public Position getPosition()
	{
		logger.debug("getPosition");

		return this.position;
	}
	
	@Override
	public void setPosition(Position newPosition)
	{
		this.position = newPosition;
	}
}
