package mazes.game.model;

import java.util.Observable;

import mazes.helper.Position;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

/**
 * Part of the Model for the maze running game.
 * It is seperate from Maze to allow it to work properly in the MVC pattern.
 * MVC:
 *   M: Player + MazeModel
 *   V: MazeView
 *   C: GameController
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class MazeModel extends Observable implements Maze
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MazeModel.class);
	
	private Maze maze;
	
	public MazeModel(Maze maze)
	{
		this.maze = maze;
	}
	
	public void changeMaze(Maze newMaze)
	{
		setChanged();
		notifyObservers(newMaze);
		this.maze = newMaze;
	}

	@Override
	public boolean getCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		logger.debug("getCell");
		return this.maze.getCell(p);
	}

	@Override
	public void setCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		logger.debug("setCell");
		this.maze.setCell(p);
	}

	@Override
	public void delCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		logger.debug("delCell");
		this.maze.delCell(p);
	}

	@Override
	public boolean getPath(Position from, Position to)
			throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		logger.debug("getPath");
		return this.maze.getPath(from, to);
	}

	@Override
	public void setPath(Position from, Position to)
			throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		logger.debug("setPath");
		this.maze.setPath(from, to);
	}

	@Override
	public void delPath(Position from, Position to)
			throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		logger.debug("delPath");
		this.maze.delPath(from, to);
	}

	@Override
	public int getWidth()
	{
		logger.debug("getWidth");
		return this.maze.getWidth();
	}

	@Override
	public int getHeight()
	{
		logger.debug("getHeight");
		return this.maze.getHeight();
	}
}
