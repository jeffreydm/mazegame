package mazes.game.model;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observable;
import java.util.Observer;

import mazes.helper.Position;
import mazes.helper.Positionable;

/**
 * Part of the Model for the maze running game.
 * MVC:
 *   M: Player + MazeModel
 *   V: MazeView
 *   C: GameController
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class Player extends Observable implements KeyListener, Positionable
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Player.class);
	
	// the player's current position
	private Position position;
	
	public Player(Position position, Observer observer)
	{
		this(position);
		logger.info("Adding observer to new Player: "+observer);
		this.addObserver(observer);
	}
	public Player(Position position, Observer[] observers)
	{
		this(position);
		logger.info("Adding list of observers to new Player: "+observers);
		for(Observer observer : observers)
			this.addObserver(observer);
	}
	public Player(Position position)
	{
		logger.info("New Player created at "+position.toString());
		this.position = position;
	}

	@Override
	public void setPosition(Position newPosition)
	{
		this.position = newPosition;
	}
	public void move(Position newPosition)
	{
		setChanged();
		notifyObservers(newPosition);
		setPosition(newPosition);
	}
	@Override
	public Position getPosition()
	{
		return this.position;
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		logger.info("keyPressed "+e);
		
		this.setChanged();
		this.notifyObservers(e);
	}
	@Override
	public void keyReleased(KeyEvent e)
	{
		logger.info("keyReleased "+e);
		// do nothing
	}
	@Override
	public void keyTyped(KeyEvent e)
	{
		logger.info("keyTyped "+e);
		// do nothing
	}
}
