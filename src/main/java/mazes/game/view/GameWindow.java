package mazes.game.view;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * 
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class GameWindow extends JFrame
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GameWindow.class);
	
	//serializable, something inherited from JFrame, just using standard since I won't be serializing
	private static final long serialVersionUID = 1L;
	
	//max and min window size values to ensure half decent windows
	private final static int MIN_WIN_WIDTH = 200;
	private final static int MAX_WIN_WIDTH = 1920;
	private final static int MAX_WIN_HEIGHT = 1000;
	
	private MazeView mazeView;
	
	public GameWindow(MazeView mazeView)
	{
		this.mazeView = mazeView;
		setup();
	}
	
	private void setup()
	{
		logger.info("setup");
		
		//check if the maze will fit into our window parameters
		int tempX = this.mazeView.getWidth();
		int tempY = this.mazeView.getHeight();
		if(tempX<MIN_WIN_WIDTH)
			tempX = MIN_WIN_WIDTH;
		if(tempX>MAX_WIN_WIDTH)
			tempX = MAX_WIN_WIDTH;
		if(tempY>MAX_WIN_HEIGHT)
			tempY = MAX_WIN_HEIGHT;


		this.setTitle("Maze Game");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		this.setContentPane(mazeView);
		this.mazeView.setPreferredSize(new Dimension(tempX, tempY));
		this.mazeView.setOpaque(true);
		this.pack();
	}
}
