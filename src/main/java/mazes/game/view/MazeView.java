package mazes.game.view;

import java.awt.Color;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLayeredPane;

import mazes.display.MazePanel;
import mazes.game.model.MazeModel;
import mazes.game.model.Player;
import mazes.helper.Position;
import mazes.helper.Positionable;
import mazes.storage.Maze;

/**
 * View for the maze running game.
 * It is seperate from Maze to allow it to work properly in the MVC pattern.
 * MVC:
 *   M: Player + MazeModel
 *   V: MazeView
 *   C: GameController
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class MazeView extends JLayeredPane implements Observer, Runnable
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MazeView.class);
	
	//serializable, something inherited from JFrame, just using standard since I won't be serializing
	private static final long serialVersionUID = 1L;

	private Thread thread;
	private volatile boolean running;
	private long timeStamp;
	
	private MazePanel mazePanel;
	private EffectPanel effectPanel;

	public MazeView(Maze maze, HashMap<Positionable, Color> objects)
	{
		start();
		
		this.mazePanel = new MazePanel(maze);
		this.effectPanel = new EffectPanel(mazePanel, objects);
		
		this.setSize(mazePanel.getWidth(), mazePanel.getHeight());
		this.setLayer(mazePanel, JLayeredPane.DEFAULT_LAYER);
		this.setLayer(effectPanel, JLayeredPane.PALETTE_LAYER);
		this.add(mazePanel);
		this.add(effectPanel);
	}
	
	@Override
	public void update(Observable o, Object arg)
	{
		logger.info("update for "+o+" passing "+arg);

		if(o instanceof MazeModel && arg instanceof Maze)
			reloadMaze((Maze) arg);
		if(o instanceof Player && arg instanceof Position)
			displayMove((Player) o, (Position) arg);
	}
	
	private void reloadMaze(Maze newMaze)
	{
		logger.info("Reloading the maze");
		
		this.remove(mazePanel);
		this.setLayer(mazePanel, JLayeredPane.DEFAULT_LAYER);
		this.mazePanel = new MazePanel(newMaze);
		this.add(mazePanel);
		
		this.effectPanel.clear();
		
		this.repaint();
	}
	
	private void displayMove(Player player, Position newPosition)
	{
		logger.info("Displaying move");
		
		this.effectPanel.drawTrail(player.getPosition(), newPosition);
	}

	public void start()
	{
		this.running = true;
		this.thread = new Thread();
		this.thread.start();
		this.timeStamp = System.currentTimeMillis();
	}
	public void stop()
	{
		logger.info("Stopping the thread");
		
		this.running = false;
		try
		{
			this.thread.join();
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void run()
	{
		logger.debug("run");
		
		while(running)
		{
			long check = System.currentTimeMillis()-timeStamp;
			if(check > 59)
				doStuff();
			else if(check < 55)
			{
				try
				{
					this.thread.sleep(55);
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private void doStuff()
	{
		this.repaint();
	}
}
