package mazes.game.view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JPanel;

import mazes.display.MazePanel;
import mazes.helper.Position;
import mazes.helper.Positionable;

public class EffectPanel extends JPanel
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EffectPanel.class);
	
	//serializable, something inherited from JFrame, just using standard since I won't be serializing
	private static final long serialVersionUID = 1L;
	
	private static final int INCREMENTAL_DEATH_OFFSET = 100;
	private static final int INITIAL_DEATH_OFFSET = INCREMENTAL_DEATH_OFFSET+130;
	
	private MazePanel referencePanel;
	private HashMap<Positionable, Color> objects;
	private int width, height;
	private Color trailColor = Color.BLUE;
	private List<TrailPiece> trails = new ArrayList<>();

	private class TrailPiece implements Positionable
	{
		private long death;
		private Position position;
		
		TrailPiece(long death, Position position)
		{
			this.death = death;
			this.position = position;
		}
		
		public long getDeath()
		{
			logger.debug("getDeath");
			
			return this.death;
		}
		
		@Override
		public Position getPosition()
		{
			logger.debug("getPosition");

			return this.position;
		}

		@Override
		public void setPosition(Position newPosition)
		{
			logger.debug("setPosition");

			this.position = newPosition;
		}
	}
	
	public EffectPanel(MazePanel referencePanel, HashMap<Positionable, Color> objects)
	{
		super();
		
		logger.info("Created a new ObjectPanel using "+referencePanel+" as a reference, objects: "+objects);
		
		this.objects = objects;
		this.referencePanel = referencePanel;
		this.width = this.referencePanel.getWidth()-this.referencePanel.getBorderSize();
		this.height = this.referencePanel.getHeight()-this.referencePanel.getBorderSize();
	}
	
	public void clear()
	{
		for(TrailPiece trail:this.trails)
		{
			this.objects.remove(trail);
		}
		this.trails.removeAll(trails);
	}
	
	//magical method that gets called automagically somewhere, draws the maze
	public void paint(Graphics g)
	{
		logger.debug("painting "+g);
		removeDeadTrails();
		
		for(Positionable object : objects.keySet())
		{			
			drawCube(g, object, objects.get(object));
		}
	}
	
	private void drawCube(Graphics g, Positionable p, Color c)
	{
		g.setColor(c);
		g.fillRect(referencePanel.getGraphicalPosition(p.getPosition()).getX(),
				referencePanel.getGraphicalPosition(p.getPosition()).getY(),
				referencePanel.getCellSize(), referencePanel.getCellSize());
	}
	
	private void removeDeadTrails()
	{
		logger.debug("removing dead trails");
		
		List<TrailPiece> deadTrails = new ArrayList<>();
		for(TrailPiece trail : trails)
		{
			if(trail.getDeath()<System.currentTimeMillis())
			{
				deadTrails.add(trail);
				objects.remove(trail);
			}
		}
		trails.removeAll(deadTrails);
	}
	
	public void drawTrail(Positionable start, Positionable end)
	{
		logger.info("draw a trail from "+start.getPosition().toString()+" to "+end.getPosition().toString());
		
		int xStart = start.getPosition().getX();
		int xEnd = end.getPosition().getX();
		int yStart = start.getPosition().getY();
		int yEnd = end.getPosition().getY();
		
		if(xStart==xEnd && yStart!=yEnd)
		{
			//vertical trail
			int pathLength = yEnd - yStart;
			logger.info("The path is a vertical segment going "+pathLength+" steps");
			
			for(int i=0; i!=pathLength;)
			{
				createTrailPiece(Math.abs(i), new Position(xStart,yStart+i));
				if(pathLength>0)
					i++;
				else
					i--;
			}
		}
		else if(yStart==yEnd && xStart!=xEnd)
		{
			//horizontal trail
			int pathLength = xEnd - xStart;
			logger.info("The path is a horizontal segment going "+pathLength+" steps");
			
			for(int i=0; i!=pathLength;)
			{
				createTrailPiece(Math.abs(i), new Position(xStart+i,yStart));
				if(pathLength>0)
					i++;
				else
					i--;
			}
		}
		else if(start.getPosition() == end.getPosition())
		{
			logger.error("Trail start was the same as its end");
		}
		else
		{
			logger.error("Trail was diagonal");
		}
		repaint();
	}
	
	private void createTrailPiece(int deathOffset, Position p)
	{
		TrailPiece newTrailPiece = new TrailPiece(System.currentTimeMillis()+INITIAL_DEATH_OFFSET+this.trails.size()*INCREMENTAL_DEATH_OFFSET, p);
		this.addObject(newTrailPiece, trailColor);
		this.trails.add(newTrailPiece);
		
	}

	public void addObject(Positionable object, Color color)
	{
		this.objects.put(object, color);
	}
	public void removeObject(Positionable object)
	{
		this.objects.remove(object);
	}
	
	// GETTERS
	
	public int getWidth()
	{
		return this.width;
	}
	public int getHeight()
	{
		return this.height;
	}
}

