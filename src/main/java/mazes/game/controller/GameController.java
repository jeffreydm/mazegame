package mazes.game.controller;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ThreadLocalRandom;

import mazes.game.model.GameMode;
import mazes.game.model.Goal;
import mazes.game.model.MazeModel;
import mazes.game.model.Player;
import mazes.game.view.GameWindow;
import mazes.game.view.MazeView;
import mazes.generation.DepthFirstMazeGenerator;
import mazes.generation.MazeGenerator;
import mazes.helper.Direction;
import mazes.helper.MazeGenerationFailureException;
import mazes.helper.Position;
import mazes.helper.Positionable;
import mazes.storage.ArrayMaze;
import mazes.storage.Maze;
import mazes.storage.helper.InvalidMazeSizeException;

/**
 * Controller for the maze running game.
 * MVC:
 *   M: Player + MazeModel
 *   V: MazeView
 *   C: GameController
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class GameController implements Observer
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GameController.class);
	
	private static final GameMode DEFAULT_MODE = GameMode.SPRINT;
	
	protected MazeGenerator generator;
	protected GameWindow gameWindow;
	protected MazeView mazeView;
	protected MazeModel mazeModel;

	protected Player player;
	protected Goal goal;
	
	protected GameMode currentMode;
	protected boolean explosive;
	protected int width = 5;
	protected int height = 5;
		
	public GameController() throws InvalidMazeSizeException, MazeGenerationFailureException
	{
		this(DEFAULT_MODE);
	}
	public GameController(GameMode m) throws InvalidMazeSizeException, MazeGenerationFailureException
	{
		logger.info("Create new GameController using GameMode "+m);
		
		setup(m);
	}
	
	protected void setup(GameMode m) throws InvalidMazeSizeException, MazeGenerationFailureException
	{
		logger.debug("setup for "+m);
		
		this.currentMode = m;
		this.width = currentMode.getWidth();
		this.height = currentMode.getHeight();
		this.explosive = currentMode.getExplosions();
		
		this.generator = new DepthFirstMazeGenerator();
		Maze maze = new ArrayMaze(width, height);
		this.generator.generate(maze);
		
		player = new Player(new Position(getRandomNumber(width), getRandomNumber(height)));
		goal = new Goal(new Position(getRandomNumber(width), getRandomNumber(height)));
		
		HashMap<Positionable, Color> objects = new HashMap<>();
		objects.put(player, Color.RED);
		objects.put(goal, Color.GREEN);
		
		mazeModel = new MazeModel(maze);
		
		mazeView = new MazeView(maze, objects);
		(new Thread(mazeView)).start();
		gameWindow = new GameWindow(mazeView);
		
		gameWindow.addKeyListener(player);
		player.addObserver(this);
		
		mazeModel.addObserver(mazeView);
		player.addObserver(mazeView);
	}
	
	public void play()
	{
		logger.info("playing :D");
	}
	
	protected void press(Direction d)
	{
		logger.debug("Moving in direction: "+d);
		
		//if the direction we tried didn't blow us up, go in that direction until the next intersection
		Position p = this.player.getPosition();
		if(check(p, d))
			slide(p, d);
		else
			explode();
	}
	
	protected void slide(Position p, Direction d)
	{
		logger.info("Moving from "+player.getPosition().toString()+" in direction "+d);
		
		if(check(p, d))
			p = new Position(p.getX()+d.getX(), p.getY()+d.getY());
		while(check(p, d) && noSidePaths(p, d))
		{
			if(p.getX() == goal.getPosition().getX() && p.getY() == goal.getPosition().getY())
				mazeEnded();
			p = new Position(p.getX()+d.getX(), p.getY()+d.getY());
		}
		if(p.getX() == goal.getPosition().getX() && p.getY() == goal.getPosition().getY())
			mazeEnded();
		else
			this.player.move(p);
	}
	
	protected boolean check(Position p, Direction d)
	{
		logger.debug("Check if "+d+" is ok from "+player.getPosition().toString());
		
		Position to = new Position(p.getX()+d.getX(),p.getY()+d.getY());
		try
		{
			if(this.mazeModel.getPath(p, to))
			{
				logger.debug("The Direction checked out");
				return true;
			}
			else
			{
				logger.debug("The Direction wasn't possible");
				return false;
			}
		} catch (Exception e)
		{
			logger.debug("Could not getPath from "+p.toString()+" to "+to.toString());
			//e.printStackTrace();
			return false;
		}
	}
	
	protected boolean noSidePaths(Position p, Direction d)
	{
		for(Direction direction : Direction.all())
		{
			if(direction!=d && direction!=Direction.oppositeOf(d))
			{
				if(check(p, direction))
					return false;
			}
		}
		
		return true;
	}
	
	protected void explode()
	{
		logger.info("You exploded.");
		
		if(explosive)
			mazeEnded();
	}
	
	protected void mazeEnded()
	{
		logger.info("The maze has ended\n");
		
		Maze newMaze;
		try
		{
			newMaze = new ArrayMaze(width, height);
			generator.generate(newMaze);
			mazeModel.changeMaze(newMaze);
			mazeView.update(mazeModel, newMaze);
		} catch (Exception e)
		{
			logger.info("Could not create new maze");
			e.printStackTrace();
			quit();
		}
		
		Position newPlayerPosition = new Position(getRandomNumber(width), getRandomNumber(height));
		Position newGoalPosition = new Position(getRandomNumber(width), getRandomNumber(height));
		while(newPlayerPosition.getX()==newGoalPosition.getX() && newPlayerPosition.getY()==newGoalPosition.getY())
		{
			newGoalPosition = new Position(getRandomNumber(width), getRandomNumber(height));
		}
		player.setPosition(newPlayerPosition);
		goal.setPosition(newGoalPosition);
	}
	
	protected int getRandomNumber(int max)
	{
		return ThreadLocalRandom.current().nextInt(1, max + 1);
	}

	@Override
	public void update(Observable arg0, Object event)
	{
		logger.info("Key was pressed");
		
		if( !(this.currentMode.getType()<0) && event instanceof KeyEvent )
		{
			switch(((KeyEvent) event).getKeyCode())
			{
				case KeyEvent.VK_ESCAPE:
				case KeyEvent.VK_DELETE:
				case KeyEvent.VK_BACK_SPACE:
					quit();
					break;
				case KeyEvent.VK_W:
				case KeyEvent.VK_UP:
					press(Direction.UP);
					break;
				case KeyEvent.VK_D:
				case KeyEvent.VK_RIGHT:
					press(Direction.RIGHT);
					break;
				case KeyEvent.VK_S:
				case KeyEvent.VK_DOWN:
					press(Direction.DOWN);
					break;
				case KeyEvent.VK_A:
				case KeyEvent.VK_LEFT:
					press(Direction.LEFT);
					break;
			}
			this.gameWindow.repaint();
		}
	}
	
	protected void quit()
	{
		this.gameWindow.dispose();
	}
}
