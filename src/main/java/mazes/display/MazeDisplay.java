package mazes.display;

import mazes.storage.Maze;

/**
 * An overarching Maze Display interface.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public interface MazeDisplay
{
	/**
	 * Displays the maze using the implementations logic.
	 * 
	 * @param maze The maze that will read from and displayed.
	 */
	public void display(Maze maze);
}
