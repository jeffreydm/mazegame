package mazes.display.export;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mazes.display.MazePanel;
import mazes.storage.Maze;

public class PNGMazeExport implements MazeExport
{
	// CLASS FIELDS
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PNGMazeExport.class);
	
	
	// OBJECT FIELDS
	
	private MazePanel panel;
	
	
	// IMPLEMENTED METHODS
	
	@Override
	public void export(Maze maze, String path)
	{
		logger.info("export "+maze+" to "+path);
		
		
		panel = new MazePanel(maze);
		BufferedImage pic = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_ARGB);
		panel.paint(pic.getGraphics());
		try
		{
			ImageIO.write(pic, "PNG", new File(path));
			logger.info("Picture exported");
		} catch (IOException e)
		{
			logger.error("Picture could not be exported");
			e.printStackTrace();
		}
	}
}
