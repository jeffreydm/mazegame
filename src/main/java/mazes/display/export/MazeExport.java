package mazes.display.export;

import mazes.storage.Maze;

/**
 * Interface for exporting mazes.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public interface MazeExport
{
	/**
	 * Export the maze.
	 * 
	 * @param maze The maze to export.
	 * @param path Where the maze should be exported.
	 */
	public void export(Maze maze, String path);
}
