package mazes.display;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import mazes.helper.Position;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

public class MazePanel extends JPanel
{
	//CLASS FIELDS
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MazePanel.class);
	
	//serializable, something inherited from JPanel, just using standard since I won't be serializing
	private static final long serialVersionUID = 1L;
	
	
	// OBJECT FIELDS
	
	private int cellSize = 20;
	private int borderSize = 5;
	private Color backgroundColor = Color.BLACK;
	private Color foregroundColor = Color.WHITE;
	
	private Maze maze;
	private int mazeWidth, mazeHeight;
	private int width, height;
	
	public MazePanel(Maze maze, Color backgroundColor, Color foregroundColor, int cellSize, int borderSize)
	{
		this(maze, backgroundColor, foregroundColor);
		this.cellSize = cellSize;
		this.borderSize = borderSize;
	}
	public MazePanel(Maze maze, Color backgroundColor, Color foregroundColor)
	{
		this(maze);
		this.backgroundColor = backgroundColor;
		this.foregroundColor = foregroundColor;
	}
	public MazePanel(Maze maze, int cellSize, int borderSize)
	{
		this(maze);
		this.cellSize = cellSize;
		this.borderSize = borderSize;
	}
	public MazePanel(Maze maze)
	{
		super();
		this.maze = maze;
		this.mazeWidth = this.maze.getWidth();
		this.mazeHeight = this.maze.getHeight();
		this.width = (this.maze.getWidth())*(cellSize+borderSize)+borderSize;
		this.height = (this.maze.getHeight())*(cellSize+borderSize)+borderSize;
	}
	
	//magical method that gets called automagically somewhere, draws the maze
	public void paint(Graphics g)
	{
		logger.debug("painting "+g);
		
		//fill background
		g.setColor(backgroundColor);
		g.fillRect(0, 0, (mazeWidth*(cellSize+borderSize)+borderSize), (mazeHeight*(cellSize+borderSize)+borderSize));
		g.setColor(foregroundColor);
		
		for(int h=0; h<mazeHeight-1;)
		{
			//h must start at 1
			h++;
			
			/* unfortunately, rows need a separate function so that I can call them once for the last
			 * row, without calling the column display, which would no longer exist, since that would
			 * be extending outside of the maze.
			 */ 
			row(g, h, mazeWidth);
			
			//add vertical paths
			for(int w=0; w<mazeWidth;)
			{
				//w must start at 1
				w++;
				
				//define from-to positions
				Position from = new Position(w, h);
				Position to = new Position(w, h+1);
				
				try
				{
					//if the path exists, mark it
					if(this.maze.getPath(from, to))
						verticalPath(g, from, to);
				}
				catch (CoordinatesOutOfMazeBoundsException e)
				{
					logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were invalid");
					e.printStackTrace();
				}
				catch (CoordinatesNotConnectedException e)
				{
					logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were not connected");
					e.printStackTrace();
				}
			}
		}
		
		//display the final row, without display non-existant columns
		row(g, mazeHeight, mazeWidth);
	}
	
	private void row(Graphics g, int y, int width)
	{
		//draw the node if it exists
		Position node = new Position(1, y);
		try
		{
			if(this.maze.getCell(node))
			{
				cell(g, node);
			}
		} catch (CoordinatesOutOfMazeBoundsException e1)
		{
			e1.printStackTrace();
			logger.error("Requested Coordinates: "+node.toString()+" were invalid");
		}
		
		//add horizontal paths
		for(int w=0; w<width-1;)
		{
			//w must start at 1
			w++;
			
			//define from-to positions
			Position from = new Position(w, y);
			Position to = new Position(w+1, y);
			
			try
			{
				//if the path exists, mark it
				if(this.maze.getPath(from, to))
					horizontalPath(g, from, to);
			}
			catch (CoordinatesOutOfMazeBoundsException e)
			{
				logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were invalid");
				e.printStackTrace();
			}
			catch (CoordinatesNotConnectedException e)
			{
				logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were not connected");
				e.printStackTrace();
			}
			
			//draw the node if it exists
			node = new Position(w, y);
			try
			{
				if(this.maze.getCell(node))
				{
					cell(g, node);
				}
			} catch (CoordinatesOutOfMazeBoundsException e1)
			{
				e1.printStackTrace();
				logger.error("Requested Coordinates: "+node.toString()+" were invalid");
			}
		}
		
		//draw the node if it exists
		node = new Position(width, y);
		try
		{
			if(this.maze.getCell(node))
			{
				cell(g, node);
			}
		} catch (CoordinatesOutOfMazeBoundsException e1)
		{
			e1.printStackTrace();
			logger.error("Requested Coordinates: "+node.toString()+" were invalid");
		}
	}
	
	//draw one cell
	private void cell(Graphics g, Position p)
	{
		Position pos = getGraphicalPosition(p);
		g.fillRect(pos.getX(), pos.getY(), cellSize, cellSize);
	}
	//draw a path from a top cell to a bottom cell
	private void verticalPath(Graphics g, Position from, Position to)
	{
		Position p1 = getGraphicalPosition(from);
		g.fillRect(p1.getX(), p1.getY()+cellSize, cellSize, borderSize);
	}
	//draw a path from a left cell to a right cell
	private void horizontalPath(Graphics g, Position from, Position to)
	{
		Position p1 = getGraphicalPosition(from);
		g.fillRect(p1.getX()+cellSize, p1.getY(), borderSize, cellSize);
	}
	//get the graphical equivalent to a cell's position
	public Position getGraphicalPosition(Position p) {
		return new Position((p.getX()-1)*(borderSize+cellSize)+borderSize, (p.getY()-1)*(borderSize+cellSize)+borderSize);
	}

	
	// GETTERS
	
	public int getWidth()
	{
		return this.width;
	}
	public int getHeight()
	{
		return this.height;
	}
	public int getCellSize()
	{
		return this.cellSize;
	}
	public int getBorderSize()
	{
		return this.borderSize;
	}
}
