package mazes.display;

import mazes.helper.Position;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

/**
 * A MazeDisplay using a simple text output.
 * Assumes 0,0 is top left.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class TextMazeDisplay implements MazeDisplay
{
	// CLASS FIELDS
	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextMazeDisplay.class);
	
	private static final String CELL = "XX";
	private static final String EMPTY = "  ";
	private static final String NEW_LINE = "\n";
	private static final String END = "\n\n";
	
	
	// OBJECT FIELDS
	
	private Maze maze;
	private int mazeHeight, mazeWidth;
	private StringBuilder display;

	
	// IMPLEMENTED METHODS
	
	@Override
	public void display(Maze maze)
	{
		logger.info("display "+maze.toString());

		display = new StringBuilder();
		this.maze = maze;
		this.mazeHeight = this.maze.getHeight();
		this.mazeWidth = this.maze.getWidth();
		build();
		write();
	}
	
	
	// PRIVATE HELPER METHODS
	
	//write the "display" to the console
	private void write()
	{
		System.out.print(display);
	}

	//build the maze string
	private void build()
	{
		for(int h=0; h<mazeHeight-1;)
		{
			//h must start at 1
			h++;
			
			/* unfortunately, rows need a separate function so that I can call them once for the last
			 * row, without calling the column display, which would no longer exist, since that would
			 * be extending outside of the maze.
			 */ 
			row(h, mazeWidth);
			
			//add vertical paths
			newLine();
			for(int w=0; w<mazeWidth;)
			{
				//w must start at 1
				w++;
				
				//define from-to positions
				Position from = new Position(w, h);
				Position to = new Position(w, h+1);
				
				try
				{
					//if the path exists, mark it
					if(this.maze.getPath(from, to))
						cell();
					else
						space();
					
					//create spacing between downward paths
					space();
				}
				catch (CoordinatesOutOfMazeBoundsException e)
				{
					logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were invalid");
					e.printStackTrace();
				}
				catch (CoordinatesNotConnectedException e)
				{
					logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were not connected");
					e.printStackTrace();
				}
			}
			
			newLine();
		}
		
		//display the final row, without display non-existant columns
		row(mazeHeight, mazeWidth);
		
		end();
	}
	
	private void row(int y, int width)
	{
		//draw the node if it exists
		Position node = new Position(1, y);
		try
		{
			if(this.maze.getCell(node))
			{
				cell();
			}
		} catch (CoordinatesOutOfMazeBoundsException e1)
		{
			e1.printStackTrace();
			logger.error("Requested Coordinates: "+node.toString()+" were invalid");
		}
		
		//add horizontal paths
		for(int w=0; w<width-1;)
		{
			//w must start at 1
			w++;
			
			//define from-to positions
			Position from = new Position(w, y);
			Position to = new Position(w+1, y);
			
			try
			{
				//if the path exists, mark it
				if(this.maze.getPath(from, to))
					cell();
				else
					space();
			}
			catch (CoordinatesOutOfMazeBoundsException e)
			{
				logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were invalid");
				e.printStackTrace();
			}
			catch (CoordinatesNotConnectedException e)
			{
				logger.error("Requested Coordinates: "+from.toString()+" -> "+to.toString()+" were not connected");
				e.printStackTrace();
			}
			
			//draw the node if it exists
			node = new Position(w, y);
			try
			{
				if(this.maze.getCell(node))
				{
					cell();
				}
			} catch (CoordinatesOutOfMazeBoundsException e1)
			{
				e1.printStackTrace();
				logger.error("Requested Coordinates: "+node.toString()+" were invalid");
			}
		}
	}
	
	private void cell()
	{
		//System.out.print(CELL);
		display.append(CELL);
	}
	private void space()
	{
		//System.out.print(EMPTY);
		display.append(EMPTY);
	}
	private void newLine()
	{
		//System.out.print(NEW_LINE);
		display.append(NEW_LINE);
	}
	private void end()
	{
		//System.out.print(ENDING);
		display.append(END);
	}
}
