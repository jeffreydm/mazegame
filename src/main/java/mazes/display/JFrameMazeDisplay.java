package mazes.display;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import mazes.helper.Position;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

/**
 * MazeDisplay using a JFrame to graphically represent a maze.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class JFrameMazeDisplay extends JFrame implements MazeDisplay
{
	// CLASS FIELDS

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextMazeDisplay.class);
	
	//serializable, something inherited from JFrame, just using standard since I won't be serializing
	private static final long serialVersionUID = 1L;
	
	//max and min window size values to ensure half decent windows
	private final static int MIN_WIN_WIDTH = 200;
	private final static int MAX_WIN_WIDTH = 1920;
	private final static int MAX_WIN_HEIGHT = 1000;
	
	
	// OBJECT FIELDS
	
	private MazePanel panel;
	
	
	// IMPLEMENTED METHODS
	
	@Override
	public void display(Maze maze)
	{
		logger.info("display "+maze);
		
		this.panel = new MazePanel(maze);
		setup();
	}
	
	//cooler version of the main method
	public void display(Maze maze, Color backgroundColor, Color foregroundColor)
	{
		logger.info("display "+maze+" using colors: "+backgroundColor+", "+foregroundColor);
		this.panel = new MazePanel(maze, backgroundColor, foregroundColor);
		setup();
	}
	
	
	// PRIVATE HELPER METHODS
	
	private void setup()
	{
		logger.info("setup");
		//check if the maze will fit into our window parameters
		int tempX = this.panel.getWidth();
		int tempY = this.panel.getHeight();
		if(tempX<MIN_WIN_WIDTH)
			tempX = MIN_WIN_WIDTH;
		if(tempX>MAX_WIN_WIDTH)
			tempX = MAX_WIN_WIDTH;
		if(tempY>MAX_WIN_HEIGHT)
			tempY = MAX_WIN_HEIGHT;


		this.setTitle("Display Maze");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		
		this.setContentPane(this.panel);
		this.panel.setPreferredSize(new Dimension(tempX, tempY));
		this.panel.setOpaque(true);
		this.pack();
	}
	
	/* Structure of my maze
	 * 
	 * OXXXOXXXO-
	 * XAAAXAAAX-
	 * XAAAXAAAX-
	 * XAAAXAAAX-
	 * OXXXOXXXO-
	 * XAAAXAAAX-
	 * XAAAXAAAX-
	 * XAAAXAAAX-
	 * OXXXOXXXO-
	 * |||||||||\
	 * 
	 * O: this is always wall
	 * A: this is always clear
	 * X: clear if the path between them is clear
	 * So basically, you fill everything with a background color, then clear out dots for every cell
	 * then, just remove where there's a path, ezpz
	 */
}
