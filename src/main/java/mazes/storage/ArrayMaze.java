package mazes.storage;

import mazes.helper.Position;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;
import mazes.storage.helper.InvalidMazeSizeException;

/**
 * Realization of Maze using Arrays.
 * 
 * @author Jeffrey Maier
 * @version 2019-July
 */
public class ArrayMaze implements Maze
{
	private int width, height;
	private boolean[][] maze;
	
	public ArrayMaze(int width, int height) throws InvalidMazeSizeException
	{
		if(width<1 || height<1)
			throw new InvalidMazeSizeException();
		this.width = width;
		this.height = height;
		maze = new boolean[2*this.width-1][2*this.height-1];
	}

	@Override
	public boolean getCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		if(!checkIfValidPosition(p))
			throw new CoordinatesOutOfMazeBoundsException();
		int x = p.getX();
		int y = p.getY();
		
		return maze[2*(x-1)][2*(y-1)];
	}

	@Override
	public void setCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		if(!checkIfValidPosition(p))
			throw new CoordinatesOutOfMazeBoundsException();
		int x = p.getX();
		int y = p.getY();
		
		maze[2*(x-1)][2*(y-1)] = true;
	}

	@Override
	public void delCell(Position p) throws CoordinatesOutOfMazeBoundsException
	{
		if(!checkIfValidPosition(p))
			throw new CoordinatesOutOfMazeBoundsException();
		int x = p.getX();
		int y = p.getY();
		
		maze[2*(x-1)][2*(y-1)] = false;
	}

	@Override
	public boolean getPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		if(!checkIfPositionsConnected(from, to))
			throw new CoordinatesNotConnectedException();
		if(!checkIfValidPosition(from))
			throw new CoordinatesOutOfMazeBoundsException();
		int x1 = from.getX();
		int y1 = from.getY();
		if(!checkIfValidPosition(to))
			throw new CoordinatesOutOfMazeBoundsException();
		int x2 = to.getX();
		int y2 = to.getY();
		
		
		int x12 = ( 2*(x1-1)+2*(x2-1) ) / 2;
		int y12 = ( 2*(y1-1)+2*(y2-1) ) / 2;
		return maze[x12][y12];
	}

	@Override
	public void setPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		if(!checkIfPositionsConnected(from, to))
			throw new CoordinatesNotConnectedException();
		if(!checkIfValidPosition(from))
			throw new CoordinatesOutOfMazeBoundsException();
		int x1 = from.getX();
		int y1 = from.getY();
		if(!checkIfValidPosition(to))
			throw new CoordinatesOutOfMazeBoundsException();
		int x2 = to.getX();
		int y2 = to.getY();
		
		int x12 = ( 2*(x1-1)+2*(x2-1) ) / 2;
		int y12 = ( 2*(y1-1)+2*(y2-1) ) / 2;
		maze[x12][y12] = true;
	}

	@Override
	public void delPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException
	{
		if(!checkIfPositionsConnected(from, to))
			throw new CoordinatesNotConnectedException();
		if(!checkIfValidPosition(from))
			throw new CoordinatesOutOfMazeBoundsException();
		int x1 = from.getX();
		int y1 = from.getY();
		if(!checkIfValidPosition(to))
			throw new CoordinatesOutOfMazeBoundsException();
		int x2 = to.getX();
		int y2 = to.getY();
		
		int x12 = ( 2*(x1-1)+2*(x2-1) ) / 2;
		int y12 = ( 2*(y1-1)+2*(y2-1) ) / 2;
		maze[x12][y12] = false;
	}
	
	private boolean checkIfValidPosition(Position p)
	{
		if(p.getX()<1 || p.getY()<1 || p.getX()>width || p.getY()>height)
			return false;
		return true;
	}
	private boolean checkIfPositionsConnected(Position from, Position to)
	{
		if(Math.abs(from.getX()-to.getX()) > 1)
			return false;
		if(Math.abs(from.getY()-to.getY()) > 1)
			return false;
		return true;
	}

	@Override
	public int getWidth()
	{
		return this.width;
	}

	@Override
	public int getHeight()
	{
		return this.height;
	}
}
