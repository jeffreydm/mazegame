package mazes.storage;

import mazes.helper.Position;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;
import mazes.storage.helper.CoordinatesNotConnectedException;

/**
 * A 2D Maze.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public interface Maze
{
	/**
	 * TODO
	 * 
	 * @param p
	 * @return
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public boolean getCell(Position p) throws CoordinatesOutOfMazeBoundsException;
	
	/**
	 * TODO
	 * 
	 * @param p
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public void setCell(Position p) throws CoordinatesOutOfMazeBoundsException;
	
	/**
	 * TODO
	 * 
	 * @param p
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public void delCell(Position p) throws CoordinatesOutOfMazeBoundsException;
	
	/**
	 * TODO
	 * 
	 * @param from
	 * @param to
	 * @return
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public boolean getPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException;
	
	/**
	 * TODO
	 * 
	 * @param from
	 * @param to
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public void setPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException;
	
	/**
	 * TODO
	 * 
	 * @param from
	 * @param to
	 * @throws CoordinatesOutOfMazeBoundsException
	 */
	public void delPath(Position from, Position to) throws CoordinatesOutOfMazeBoundsException, CoordinatesNotConnectedException;
	
	/**
	 * TODO
	 * 
	 * @return
	 */
	public int getWidth();
	
	/**
	 * TODO
	 * 
	 * @return
	 */
	public int getHeight();
}
