package mazes.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Direction
{
	UP(0,-1),
	RIGHT(1,0),
	DOWN(0,1),
	LEFT(-1,0);
	
	protected final int x;
	protected final int y;
	
	Direction(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public static ArrayList<Direction> all()
	{
		Direction[] asArray =  new Direction[] {UP, RIGHT, DOWN, LEFT};
		List<Direction> asList = Arrays.asList(asArray);
		return new ArrayList<Direction>(asList);
	}
	public static ArrayList<Direction> shuffled()
	{
		ArrayList<Direction> toShuffle = all();
		Collections.shuffle( toShuffle );
		return toShuffle;
	}
	public static Direction oppositeOf(Direction d)
	{
		switch (d)
		{
		case UP:
			return DOWN;
		case RIGHT:
			return LEFT;
		case DOWN:
			return UP;
		case LEFT:
			return RIGHT;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
}
