package mazes.helper;

import org.slf4j.LoggerFactory;

public class Position implements Positionable
{
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Position.class);
	
	public Position(int x, int y)
	{
		logger.debug("new Position at "+x+","+y);
		
		this.x = x;
		this.y = y;
	}
	public Position(Position p)
	{
		logger.debug("new Position using "+p.toString()+" as a reference");
		
		this.x = p.getX();
		this.y = p.getY();
	}
	
	private int x;
	private int y;
	
	public boolean equal(Position p)
	{
		logger.debug("Checking if "+this+".equal("+p+") ?");
		
		if( this.x == p.getX() && this.y == p.getY() )
			return true;
		return false;
	}
	
	public String toString()
	{
		return "("+this.x+", "+this.y+")";
	}
	
	public int getX()
	{
		return x;
	}
	public void setX(int x)
	{
		this.x = x;
	}
	public void adjustX(int a)
	{
		this.x += a;
	}
	
	public int getY()
	{
		return y;
	}
	public void setY(int y)
	{
		this.y = y;
	}
	public void adjustY(int a)
	{
		this.y += a;
	}

	@Override
	public Position getPosition()
	{
		return this;
	}
	@Override
	public void setPosition(Position newPosition)
	{
		this.x = newPosition.getX();
		this.y = newPosition.getY();
	}
}
