package mazes.helper;

public interface Positionable
{
	public Position getPosition();

	public void setPosition(Position newPosition);
}
