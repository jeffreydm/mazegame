package mazes;

import mazes.game.model.GameMode;
import mazes.game.controller.GameController;

import java.util.Scanner;

public class Main {
    public static void main(String... args) throws Exception {
        System.out.print(""+
                "======================================================================\n"+
                "  Choose a gamemode:\n"+
                "    1. Sprint: Get the goal as quickly as possible\n"+
                "    2. Precision: Don't make any mistakes, or everything will reset!\n"+
                "    3. Sprawl: Just a _gigantic_ maze\n"+
                "    4. Reflex: A small fun map size\n"+
                "======================================================================\n"+
                "\n"+
                "Gamemode: "
        );
        Scanner s = new Scanner(System.in);
        int choice = s.nextInt();
        GameMode mode;
        switch (choice) {
            default:
            case 1:
                mode = GameMode.SPRINT;
                break;
            case 2:
                mode = GameMode.PRECISION;
                break;
            case 3:
                mode = GameMode.SPRAWL;
                break;
            case 4:
                mode = GameMode.REFLEX;
                break;
        }
        GameController gameController = new GameController(mode);
        gameController.play();
    }
}
