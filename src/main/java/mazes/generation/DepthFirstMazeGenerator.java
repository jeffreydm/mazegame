package mazes.generation;

import java.util.ArrayDeque;
import java.util.ArrayList;

import mazes.helper.Direction;
import mazes.helper.MazeGenerationFailureException;
import mazes.helper.Position;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

/**
 * Depth-First maze generation algorithm.
 * Creates perfect mazes.
 * (Perfect mazes have exactly one path between any two cells).
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class DepthFirstMazeGenerator implements MazeGenerator
{
	// CLASS FIELDS
	
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DepthFirstMazeGenerator.class);	
	
	private Maze maze;
	private int width, height;
	
	
	// OBJECT FIELDS
	
	private Position start, currentPosition, tempPosition;
	private ArrayDeque<Direction> moves;
	private boolean stepped;
	private ArrayList<Direction> shuffled;
		
	
	private void setUp(Maze maze)
	{
		logger.debug("setUp "+maze.toString());
		
		this.maze = maze;
		this.width = this.maze.getWidth();
		this.height = this.maze.getHeight();
	}
	
	
	// IMPLEMENTED METHODS
	
	@Override
	public void generate(Maze maze) throws MazeGenerationFailureException
	{
		logger.debug("generate");
		
		setUp(maze);
		
		start = getRandomStart();
		currentPosition = new Position(start);
		moves = new ArrayDeque<Direction>();
		try
		{
			maze.setCell(start);
		} catch (CoordinatesOutOfMazeBoundsException e)
		{
			logger.error("generate first maze set failed");
			e.printStackTrace();
			throw new MazeGenerationFailureException();
		}
		
		do {
			stepped = false;
			shuffled = Direction.shuffled();
			
			while ( !stepped && !shuffled.isEmpty() )
			{
				tempPosition = new Position(currentPosition);
				Direction step = shuffled.get(0);
				logger.debug("Direction "+step.toString()+" from "+shuffled.toString());
				shuffled.remove(step);
				
				tempPosition.adjustX(step.getX());
				tempPosition.adjustY(step.getY());
				
				if( isValidStep() )
					step(step);
			}
			
			if( !stepped )
				unstep();
			
		} while ( !currentPosition.equal(start) );
	}
	
	
	// PRIVATE HELPER METHODS
	
	private Position getRandomStart()
	{
		logger.debug("getRandomStart");
		
		int x = 1; //TODO random int
		int y = 1; //TODO random int
		return new Position(x, y);
	}
	
	private boolean isValidStep() throws MazeGenerationFailureException
	{
		logger.debug("isValidStep");
		
		try
		{
			if(	tempPosition.getX() < 1 ||
				tempPosition.getY() < 1 ||
				tempPosition.getX() > this.width ||
				tempPosition.getY() > this.height ||
				maze.getCell(tempPosition) )
			{
				logger.debug("invalid step");
				return false;
			}
		} catch (CoordinatesOutOfMazeBoundsException e)
		{
			logger.error("isValidStep failed");
			e.printStackTrace();
			throw new MazeGenerationFailureException();
		}
		
		logger.debug("valid step");
		return true;
	}
	
	private void step(Direction d) throws MazeGenerationFailureException
	{
		logger.debug("step "+d.toString()+" from "+
				"("+currentPosition.getX()+", "+currentPosition.getY()+") to ("+
				tempPosition.getX()+", "+tempPosition.getY()+")");
		stepped = true;
		moves.push(d);
		
		try
		{
			maze.setCell(tempPosition);
			maze.setPath(currentPosition, tempPosition);
		}
		catch (CoordinatesOutOfMazeBoundsException e)
		{
			logger.error("Step failed for "+d.toString()+", position "+tempPosition.toString()+" does not exist");
			e.printStackTrace();
			throw new MazeGenerationFailureException();
		}
		catch (CoordinatesNotConnectedException e)
		{
			logger.error("Step failed for "+d.toString()+", positions "+currentPosition.toString()+" -> "+tempPosition.toString()+" are not connected");
			e.printStackTrace();
			throw new MazeGenerationFailureException();
		}
		
		currentPosition = new Position(tempPosition);
	}
	
	private void unstep()
	{
		logger.debug("unstep");
		
		Direction d = moves.pop();
		
		currentPosition.adjustX(-d.getX());
		currentPosition.adjustY(-d.getY());
	}
	
	
	// GETTERS AND SETTERS
	
	public int getWidth()
	{
		return this.width;
	}
	public int getHeight()
	{
		return this.height;
	}
}
