package mazes.generation;

import mazes.helper.MazeGenerationFailureException;
import mazes.storage.Maze;

/**
 * An overarching Maze Generation interface.
 * 
 * @author Jeffrey Maier
 * @version 2019-July
 */
public interface MazeGenerator
{
	/**
	 * 
	 * @param maze The maze that the generator will write to.
	 * @throws MazeGenerationFailureException If the generator was unable to finish for any reason.
	 */
	public void generate(Maze maze) throws MazeGenerationFailureException;
}
