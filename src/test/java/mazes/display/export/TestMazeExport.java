package mazes.display.export;

import java.io.File;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mazes.generation.DepthFirstMazeGenerator;
import mazes.generation.MazeGenerator;
import mazes.storage.ArrayMaze;
import mazes.storage.Maze;

public abstract class TestMazeExport
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestMazeExport.class);

	private static final int TEST_WIDTH = 10, TEST_HEIGHT=10;
	private static final String PATH = "./test_maze_file";
	
	private static Maze maze;
	private static MazeGenerator generator;
	private static MazeExport exporter;
	
	@BeforeAll
	static void setUp() throws Exception
	{
		logger.info("setUp width: "+TEST_WIDTH+", height: "+TEST_HEIGHT);
		
		maze = new ArrayMaze(TEST_WIDTH, TEST_HEIGHT);
		generator = new DepthFirstMazeGenerator();
		generator.generate(maze);
	}
	
	@BeforeEach
	void beforeEach() throws Exception
	{
		logger.info("beforeEach");
		
		exporter = defineMazeExportType();
	}
	
	protected abstract MazeExport defineMazeExportType() throws Exception;
	
	@Test
	void testExport()
	{
		exporter.export(maze, PATH);
		File file = new File(PATH);
		assert(file.delete());
	}
}
