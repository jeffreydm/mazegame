package mazes.display;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mazes.generation.DepthFirstMazeGenerator;
import mazes.generation.MazeGenerator;
import mazes.storage.ArrayMaze;
import mazes.storage.Maze;

abstract class TestMazeDisplay
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestMazeDisplay.class);
	
	private static final int WIDTH = 10, HEIGHT=10;
	
	private static MazeDisplay display;
	private static Maze maze;
	private static MazeGenerator generator;
	
	@BeforeAll
	static void setUp() throws Exception
	{
		logger.info("setUp width: "+WIDTH+", height: "+HEIGHT);
		
		maze = new ArrayMaze(WIDTH, HEIGHT);
		generator = new DepthFirstMazeGenerator();
		generator.generate(maze);
	}
	
	@BeforeEach
	void beforeEach() throws Exception
	{
		logger.info("beforeEach");
		
		display = defineMazeDisplayType();
	}
	
	protected abstract MazeDisplay defineMazeDisplayType() throws Exception;

	@AfterEach
	void afterEach() throws Exception
	{
		logger.info("afterEach");
		
		display = null;
	}

	@Test
	void testDisplay()
	{
		display.display(maze);
	}
}