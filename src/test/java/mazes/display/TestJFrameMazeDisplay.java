package mazes.display;

public class TestJFrameMazeDisplay extends TestMazeDisplay
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestJFrameMazeDisplay.class);

	@Override
	protected MazeDisplay defineMazeDisplayType() throws Exception
	{
		logger.info("TestMazeDisplay defineMazeDisplayType: JFrame");
		
		return new JFrameMazeDisplay();
	}
}
