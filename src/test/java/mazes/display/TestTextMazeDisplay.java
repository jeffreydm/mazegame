package mazes.display;

public class TestTextMazeDisplay extends TestMazeDisplay
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestTextMazeDisplay.class);

	@Override
	protected MazeDisplay defineMazeDisplayType() throws Exception
	{
		logger.info("TestMazeDisplay defineMazeDisplayType: Text");
		
		return new TextMazeDisplay();
	}

}
