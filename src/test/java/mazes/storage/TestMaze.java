package mazes.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mazes.helper.Position;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;
import mazes.storage.helper.InvalidMazeSizeException;

/**
 * Abstract TestMaze class for testing any Maze implementations.
 * Uses a defineMazeType() to choose an implementation.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public abstract class TestMaze
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestMaze.class);
	
	protected static final int TEST_WIDTH = 5;
	protected static final int TEST_HEIGHT = 6;
	
	protected static final Position TEST_P1 = new Position(1,1);
	protected static final Position TEST_P2 = new Position(1,2);
	protected static final Position TEST_REMOVED_P1 = new Position(1,4);
	protected static final Position TEST_REMOVED_P2 = new Position(4,1);
	protected static final Position TEST_BAD_P1_CONNECTOR = new Position(5,6);
	protected static final Position TEST_BAD_P1 = new Position(6,6);
	protected static final Position TEST_BAD_P2 = new Position(5,7);
	
	protected Maze maze;
	
	@BeforeEach
	void setUp() throws Exception
	{
		logger.info("setUp with "+TEST_WIDTH+" by "+TEST_HEIGHT+" size maze");
		
		maze = defineMazeType();
	}
	
	protected abstract Maze defineMazeType() throws Exception;

	@AfterEach
	void tearDown() throws Exception
	{
		logger.info("tearDown");
	}
	
	@Test
	void testBadConstruction()
	{
		logger.info("make a maze that has no size");
		
		assertThrows(InvalidMazeSizeException.class, () -> {
            maze = new ArrayMaze(0, 0);
        });
		assertThrows(InvalidMazeSizeException.class, () -> {
            maze = new ArrayMaze(1, 0);
        });
		assertThrows(InvalidMazeSizeException.class, () -> {
            maze = new ArrayMaze(-1, -1);
        });
	}
	
	@Test
	void testFieldGetters()
	{
		logger.info("testCreation");
		
		assertEquals(TEST_HEIGHT, maze.getHeight());
		assertEquals(TEST_HEIGHT, maze.getHeight());
	}
	
	@Test
	void testGetCell() throws Exception
	{
		logger.info("testGetCell at "+TEST_P1.toString());
		
		boolean b = maze.getCell(TEST_P1);
		assertEquals(false, b);
	}
	
	@Test
	void badTestGetInvalidCell() throws Exception
	{
		logger.info("badTestGetInvalidCell at "+TEST_BAD_P1.toString());

		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getCell(new Position(0,0));
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getCell(new Position(1,0));
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getCell(new Position(10,1));
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getCell(new Position(1,10));
        });
	}
	
	@Test
	void testSetCell() throws Exception
	{
		logger.info("testSetCell at "+TEST_P1.toString());
		
		boolean b = maze.getCell(TEST_P1);
		if(!b)
		{
			maze.setCell(TEST_P1);
			assertEquals(true, maze.getCell(TEST_P1));
		}
		else
		{
			fail("The maze already contained filled cells");
		}
	}
	
	@Test
	void badTestSetInvalidCell() throws Exception
	{
		logger.info("badTestSetInvalidCell at "+TEST_BAD_P1.toString());
		
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.setCell(TEST_BAD_P1);
        });
	}
	
	@Test
	void testDeleteCell() throws Exception
	{
		logger.info("testDeleteCell at "+TEST_P1.toString());
		
		boolean b = maze.getCell(TEST_P1);
		if(!b)
		{
			maze.setCell(TEST_P1);
			b = maze.getCell(TEST_P1);
		}
		else
		{
			fail("The maze already contained filled cells");
		}
		if(b)
		{
			maze.delCell(TEST_P1);
			assertEquals(false, maze.getCell(TEST_P1));
		}
		else
		{
			fail("The maze cell was not filled");
		}
	}
	
	@Test
	void badTestDeleteInvalidCell() throws Exception
	{
		logger.info("badTestDeleteInvalidCell at "+TEST_BAD_P1.toString());
		
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.delCell(TEST_BAD_P1);
        });
	}
	
	@Test
	void testGetPath() throws Exception
	{
		logger.info("testGetPath from "+TEST_P1.toString()+" to "+TEST_P2.toString());
		
		boolean b = maze.getPath(TEST_P1, TEST_P2);
		assertEquals(false, b);
	}
	
	@Test
	void badTestGetInvalidPath() throws Exception
	{
		logger.info("badTestGetInvalidPath from "+TEST_BAD_P1.toString()+" to "+TEST_BAD_P2.toString());
		
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getPath(TEST_BAD_P1, TEST_BAD_P1);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getPath(TEST_BAD_P2, TEST_BAD_P2);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.getPath(TEST_BAD_P1_CONNECTOR, TEST_BAD_P1);
        });
	}
	
	@Test
	void badTestGetDisconnectedPath() throws Exception
	{
		logger.info("badTestGetDisconnectedPath from "+TEST_P1.toString()+" to "+TEST_REMOVED_P1.toString());
		
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.getPath(TEST_P1, TEST_REMOVED_P1);
        });
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.getPath(TEST_P1, TEST_REMOVED_P2);
        });
	}
	
	@Test
	void testSetPath() throws Exception
	{
		logger.info("testSetPath from "+TEST_P1.toString()+" to "+TEST_P2.toString());
		
		boolean b = maze.getPath(TEST_P1, TEST_P2);
		if(!b)
		{
			maze.setPath(TEST_P1, TEST_P2);
			assertEquals(true, maze.getPath(TEST_P1, TEST_P2));
		}
		else
		{
			fail("The maze already contained filled paths");
		}
	}
	
	@Test
	void badTestSetInvalidPath() throws Exception
	{
		logger.info("badTestSetInvalidPath from "+TEST_BAD_P1.toString()+" to "+TEST_BAD_P2.toString());
		
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.setPath(TEST_BAD_P1, TEST_BAD_P1);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.setPath(TEST_BAD_P2, TEST_BAD_P2);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.setPath(TEST_BAD_P1_CONNECTOR, TEST_BAD_P1);
        });
	}
	
	@Test
	void badTestSetDisconnectedPath() throws Exception
	{
		logger.info("badTestSetDisconnectedPath from "+TEST_P1.toString()+" to "+TEST_REMOVED_P1.toString());
		
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.setPath(TEST_P1, TEST_REMOVED_P1);
        });
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.setPath(TEST_P1, TEST_REMOVED_P2);
        });
	}
	
	@Test
	void testDeletePath() throws Exception
	{
		logger.info("testDeletePath from "+TEST_P1.toString()+" to "+TEST_P2.toString());
		
		boolean b = maze.getPath(TEST_P1, TEST_P2);
		if(!b)
		{
			maze.setPath(TEST_P1, TEST_P2);
			b = maze.getPath(TEST_P1, TEST_P2);
		}
		else
		{
			fail("The maze already contained filled paths");
		}
		if(b)
		{
			maze.delPath(TEST_P1, TEST_P2);
			assertEquals(false, maze.getPath(TEST_P1, TEST_P2));
		}
		else
		{
			fail("The maze path was not filled");
		}
	}
	
	@Test
	void badTestDeleteInvalidPath() throws Exception
	{
		logger.info("badTestDeleteInvalidPath from "+TEST_BAD_P1.toString()+" to "+TEST_BAD_P2.toString());
		
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.delPath(TEST_BAD_P1, TEST_BAD_P1);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.delPath(TEST_BAD_P2, TEST_BAD_P2);
        });
		assertThrows(CoordinatesOutOfMazeBoundsException.class, () -> {
            maze.delPath(TEST_BAD_P1_CONNECTOR, TEST_BAD_P1);
        });
	}
	
	@Test
	void badTestDeleteDisconnectedPath() throws Exception
	{
		logger.info("badTestDeleteDisconnectedPath from "+TEST_P1.toString()+" to "+TEST_REMOVED_P1.toString());
		
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.delPath(TEST_P1, TEST_REMOVED_P1);
        });
		assertThrows(CoordinatesNotConnectedException.class, () -> {
            maze.delPath(TEST_P1, TEST_REMOVED_P2);
        });
	}
}
