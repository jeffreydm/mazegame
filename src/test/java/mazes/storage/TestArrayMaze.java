package mazes.storage;

/**
 * ArrayMaze test using standard tests delivered by abstract class TestMaze.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class TestArrayMaze extends TestMaze
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestArrayMaze.class); 

	@Override
	protected Maze defineMazeType() throws Exception
	{
		logger.info("TestMaze defineMazeType: Array");
		
		return new ArrayMaze(TEST_WIDTH, TEST_HEIGHT);
	}

	//@Test
	//check to see if maze setting works, and watchers are notified
}
