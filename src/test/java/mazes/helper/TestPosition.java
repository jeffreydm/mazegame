/**
 * 
 */
package mazes.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * TODO add something here.
 * 
 * @author Jeffrey Maier
 * @version 2019-July
 */
class TestPosition
{
	@Test
	void testNormalConstructor()
	{
		int testX = 1;
		int testY = 2;
		Position p = new Position(testX, testY);
		Assertions.assertEquals(testX, p.getX());
		Assertions.assertEquals(testY, p.getY());
		
		testX = -1;
		testY = -2;
		p = new Position(testX, testY);
		Assertions.assertEquals(testX, p.getX());
		Assertions.assertEquals(testY, p.getY());
	}
	
	@Test
	void testSeperateConstructor()
	{
		int testX = 1;
		int testY = 2;
		Position p1 = new Position(testX, testY);
		Position p2 = new Position(p1);
		Assertions.assertEquals(testX, p2.getX());
		Assertions.assertEquals(testY, p2.getY());
	}
	
	@Test
	void testCoordinateSetting()
	{
		Position p = new Position(0, 0);
		int testX = 1;
		int testY = 2;
		p.setX(testX);
		p.setY(testY);
		Assertions.assertEquals(testX, p.getX());
		Assertions.assertEquals(testY, p.getY());
	}
	
	@Test
	void testCoordinateAdjusting()
	{
		Position p = new Position(0, 0);
		int testX = 1;
		int testY = 2;
		p.adjustX(testX);
		p.adjustY(testY);
		Assertions.assertEquals(testX, p.getX());
		Assertions.assertEquals(testY, p.getY());
	}
}
