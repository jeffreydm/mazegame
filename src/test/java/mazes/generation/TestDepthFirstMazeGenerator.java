/**
 * 
 */
package mazes.generation;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mazes.helper.Direction;
import mazes.helper.Position;
import mazes.storage.ArrayMaze;
import mazes.storage.Maze;
import mazes.storage.helper.CoordinatesNotConnectedException;
import mazes.storage.helper.CoordinatesOutOfMazeBoundsException;

/**
 * @author Jeffrey Maier
 * @version 2019-November
 */
class TestDepthFirstMazeGenerator
{	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestDepthFirstMazeGenerator.class);
	
	private final int TEST_WIDTH = 20;
	private final int TEST_HEIGHT = 21;
	
	private Maze maze;
	private MazeGenerator generator;
	private boolean[][] validationArray;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception
	{
		maze = new ArrayMaze(TEST_WIDTH, TEST_HEIGHT);
		generator = new DepthFirstMazeGenerator();
		generator.generate(maze);
		validationArray = new boolean[TEST_WIDTH][TEST_HEIGHT];
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception
	{
		maze = null;
		generator = null;
	}
	
	@Test
	void testGeneration() throws Exception
	{
		logger.info("testGeneration");
		
		//check if the entire maze is filled and has no loops
		if( takePath(new Position(0,1), Direction.RIGHT) )
		{
			
			//check that the entire maze was filled
			boolean passed = true;
			for(int i=0; i<validationArray.length; i++)
			{
				for(int j=0; j<validationArray.length; j++)
				{
					if(!validationArray[i][j])
						passed = false;
				}
			}
			
			if(!passed)
				fail("The maze was not fully filled out");
		}
		else
		{
			fail("The maze was not perfect");
		}
	}
	
	//find all possible directions we can go from our position in the maze
	private ArrayList<Direction> findPaths(Position p)
	{
		logger.info("findPaths for "+p.toString());
		
		ArrayList<Direction> returnVal = new ArrayList<Direction>();
		
		for(Direction d : Direction.all())
		{
			Position step = new Position(p.getX()+d.getX(), p.getY()+d.getY());
			try
			{
				if( maze.getPath(p, step) )
				{
					returnVal.add(d);
				}
			}
			catch (CoordinatesOutOfMazeBoundsException e)
			{
				//do nothing, this isn't unexpected
			}
			catch (CoordinatesNotConnectedException e)
			{
				logger.error("GOOD JOB! Unreachable code was reached -> findPaths "+p.toString()+" to "+step.toString());
				e.printStackTrace();
			}
		}
		
		logger.info("findPaths is returning "+returnVal.toString());
		return returnVal;
	}
	
	//follow a path in the maze, making sure that the maze is properly defined everywhere we go
	private boolean takePath(Position p, Direction path) throws CoordinatesOutOfMazeBoundsException
	{
		logger.info("Taking path "+p.toString()+" going "+path.toString());

		//define our new position
		Position here = new Position(p.getX()+path.getX(), p.getY()+path.getY());
		//check if this cell has already been visited (meaning not perfect) and that the cell exists in the maze (is properly filled)
		if(validationArray[here.getX()-1][here.getY()-1] && maze.getCell(here) )
		{
			return false;
		}
		else
		{
			//mark this cell as having been checked
			validationArray[here.getX()-1][here.getY()-1] = true;
			
			//now go in every direction FROM WHICH WE DID NOT COME and do the same check there
			Direction from = Direction.oppositeOf(path);
			for(Direction d : findPaths(here))
			{
				if(d!=from)
				{
					if(!takePath(here, d))
						return false;
				}
			}
			
			return true;
		}
	}
}
