package mazes;

import mazes.game.controller.GameController;
import mazes.game.model.GameMode;

/**
 * Demo for Mazes.
 * 
 * @author Jeffrey Maier
 * @version 2019-November
 */
public class Demo
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Demo.class);

	public static void main(String[] args) throws Exception
	{
		logger.info("start main");
		
		/*
		//setup general classes
		MazeGenerator generator = new DepthFirstMazeGenerator();
		MazeDisplay textDisplay = new TextMazeDisplay();
		MazeDisplay frameDisplay1 = new JFrameMazeDisplay();
		MazeDisplay frameDisplay2 = new JFrameMazeDisplay();
		JFrameMazeDisplay frameDisplay3 = new JFrameMazeDisplay();
		MazeExport exporter = new PNGMazeExport();
		
		//define maze dimensions
		int width = 35;
		int height = 4;
		
		//create two mazes for demonstration
		Maze maze1 = new ArrayMaze(width, height);
		Maze maze2 = new ArrayMaze(width, height);
		
		width = 20;
		height = 20;
		
		Maze maze3 = new ArrayMaze(width, height);
		
		//generate the mazes
		generator.generate(maze1);
		generator.generate(maze2);
		generator.generate(maze3);
		
		//display the first two mazes using the text display
		textDisplay.display(maze1);
		textDisplay.display(maze2);
		
		//display the third maze using the JFrame display (once general, once specific)
		frameDisplay1.display(maze2);
		frameDisplay2.display(maze3);
		frameDisplay3.display(maze3, Color.decode("#bbdece"), Color.decode("#112311"));
		
		//export the third maze
		String path = "/home/mesher/Desktop/";
		String fileName = "maze";
		exporter.export(maze3, path+fileName);
		*/
		
		//GameModes: PRECISION, REFLEX, SPRAWL, SPRINT
		GameController g = new GameController(GameMode.PRECISION);
		g.play();
	}
}