package mazes.game.model;

import mazes.storage.ArrayMaze;
import mazes.storage.Maze;
import mazes.storage.TestMaze;

public class TestMazeModel extends TestMaze
{
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TestMazeModel.class);

	@Override
	protected Maze defineMazeType() throws Exception
	{
		logger.info("TestMaze defineMazeType: MazeModel");

		return new MazeModel(new ArrayMaze(TEST_WIDTH, TEST_HEIGHT));
	}
}
